#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import cv2
import numpy as np
import pyrealsense2 as rs
import datetime
import snap7
from snap7 import util
import sys

width=1920 # image width of the realsense camera. Nominal: 1920
height=1080 # image height of the realsense camera. Nominal: 1080
rsfps=30 # fps target for realsense camera. Nominal: 30


from snap7.snap7types import *
from snap7.util import *
#M2 = client.read_area(areas['MK'],0,2,1)


print("Opening connection to PLC")

client = snap7.client.Client()

#client.set_session_password("Atlantic")
import time
while ( not client.get_connected() ):
    try:
        client.connect('172.16.11.225', 0, 2)
    except:
        
        time.sleep(1000)
        client.disconnect()
        time.sleep(1000)
        print("Connection attempt timed out")

TMCamPrev = np.load('/home/tera/t-brain/t-brain-collector/data/TMCamPrev.npy')

print("Getting pipeline...")
pipeline = rs.pipeline()
print("Getting config...")
config = rs.config()
print("Enabling stream...")
config.enable_stream(rs.stream.color, width, height, rs.format.bgr8, rsfps) # Nominal: 1920x18080 bgr8 30fps
print("Resolving pipeline...")
profile = config.resolve(pipeline)
print("Starting pipeline...")
pipeline.start(config)

print ("Press ENTER to save images")
print ("Press ESC to quit")

while True:
    # Wait for a coherent pair of frames: depth and color
    frames = pipeline.wait_for_frames()
    # depth_frame = frames.get_depth_frame()
    color_frame = frames.get_color_frame()
    if not color_frame:
        continue
    # Convert images to numpy arrays
    image_np = np.asanyarray(color_frame.get_data())
    # depth_image = np.asanyarray(depth_frame.get_data())
    #    image_np = cv2.flip(image_np, -1)
    image_np = cv2.flip(image_np, -1)
    imgBaseCamSpace = image_np

    imgBasePrevSpace = cv2.warpPerspective(imgBaseCamSpace, TMCamPrev, (1024, 1024))
    cv2.imshow("PrevSpace",imgBasePrevSpace)
    k=cv2.waitKey(50)
    if ( k == -1 ):
        mw234 = client.read_area(areas['MK'], 0, 234, S7WLWord)
        plc_sync = snap7.util.get_int(mw234, 0)
        if(plc_sync>0):
            now = datetime.datetime.now()
            fn = ("/home/tera/t-brain/t-brain-collector/data/img/" + str(int(now.timestamp() * 1000)) + ".jpg")
            print("saving img " + fn)
            cv2.imwrite(fn, imgBasePrevSpace)
            snap7.util.set_int(mw234, 0, 0)
            client.write_area(areas['MK'], 0, 234, mw234)
        continue
    if ( k == 1048586 or k==10 ):
        now = datetime.datetime.now()
        fn = ( "/home/tera/t-brain/t-brain-collector/data/shoots/" + str(int(now.timestamp()*1000)) + ".jpg" )
        print("saving img " + fn)
        cv2.imwrite( fn , imgBasePrevSpace )
        continue
    if ( k == 1048603 or k==27 ):
        print ("exiting...")
        break


pipeline.stop()
cv2.destroyAllWindows()




client.disconnect()

sys.exit(0)











